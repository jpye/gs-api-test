use REST::Client;
use JSON;

$BASE_URI = "http://pye.ninja";

$client = REST::Client->new();
register("765388613f9dbdae546b8db592b06c07");
getStatus("765388613f9dbdae546b8db592b06c07");
# Update expected the $serverAPIKey and list of api keys and their desired Status
update("DEV-KEY", "")

sub register
{
   my $apiKey = shift;
   $client->addHeader('APIKey', $apiKey);
   $client->GET($BASE_URI . '/event/register');
   print to_json(from_json($client->responseContent,  {allow_nonref=>1}),{pretty=>1});
}

sub update
{
   my $serverAPIKey = shift;
   my $clientUpdates = shift;
   $client->addHeader('APIKey', $serverAPIKey);
   $client->GET($BASE_URI . '/event/register');
   print to_json(from_json($client->responseContent,  {allow_nonref=>1}),{pretty=>1});
}

sub getStatus
{
   my $apiKey = shift;
   $client->addHeader('APIKey', $apiKey);
   $client->GET($BASE_URI . '/event/status');
   print to_json(from_json($client->responseContent,  {allow_nonref=>1}),{pretty=>1});
}
